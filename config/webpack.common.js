const WebpackOnBuildPlugin = require('on-build-webpack');
const fs = require('fs-extra');
const copy = require('copy');

const onBuild =  new WebpackOnBuildPlugin(function(stats) {
  fs.removeSync('dist');
  copy('./admin/**', 'dist/admin', () => {});
  copy('./assets/**', 'dist/assets', () => {});
  copy('./frontend/**', 'dist/frontend', () => {});
  copy('./models/**', 'dist/models', () => {});
  copy('./utils/**', 'dist/utils', () => {});
  copy('./vendors/**', 'dist/vendors', () => {});
  copy(['./*.php', './*.md', './*.txt', './config.*'], 'dist', () => {});
  copy.each([ 'LICENSE' ], 'dist', () => {});
  copy('./tmp/**', 'dist/frontend/assets', () => {});
})

module.exports = {
  onBuild
}